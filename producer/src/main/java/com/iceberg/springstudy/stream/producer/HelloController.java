package com.iceberg.springstudy.stream.producer;

import com.iceberg.springstudy.stream.common.event.CustomApplicationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.bus.BusProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/hello")
@RestController
public class HelloController {

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private BusProperties busProperties;

    @GetMapping("/test")
    public String test(@RequestParam("message") String message) {
        String id = busProperties.getId();
        applicationContext.publishEvent(new CustomApplicationEvent(this, id, null, message));
        return "发送成功！";
    }
}
