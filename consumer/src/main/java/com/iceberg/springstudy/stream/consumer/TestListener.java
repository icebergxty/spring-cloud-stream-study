package com.iceberg.springstudy.stream.consumer;

import com.iceberg.springstudy.stream.common.event.CustomApplicationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestListener implements ApplicationListener<CustomApplicationEvent> {

    @Override
    public void onApplicationEvent(CustomApplicationEvent event) {
        log.info(event.getMessage());
        System.out.println(event.getMessage());
    }
}
