package com.iceberg.springstudy.stream.common.event;

import lombok.Getter;
import org.springframework.cloud.bus.event.RemoteApplicationEvent;

public class CustomApplicationEvent extends RemoteApplicationEvent {

    @Getter
    private String message;

    public CustomApplicationEvent() {
        super();
    }

    public CustomApplicationEvent(Object source, String originService, String message) {
        super(source, originService);
        this.message = message;
    }

    public CustomApplicationEvent(Object source, String originService, String destinationService, String message) {
        super(source, originService, destinationService);
        this.message = message;
    }
}
